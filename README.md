# Docker images with `docker buildx` support

⚡ Now with `docker:20.10` support (official, non-beta/RC version). ⚡

The [official Docker images](https://hub.docker.com/_/docker) don't include support for [Buildx](https://docs.docker.com/buildx/working-with-buildx/), which is an indispensable Docker builder & CLI feature.

This repo provides (via the associated GitLab [Container Registry](https://gitlab.com/jarvis-network/base/container-images/docker-buildx/container_registry)) images based with on the official docker images, but with Buildx support added on top (and nothing else but that).

The current CI/CD matrix is the following:

```yml
matrix:
  - DOCKER_VERSION: ['latest', 'dind', '23.0.0-rc.1', '23.0.0-rc.1-dind', '20.10', '20.10-dind']
    BUILDX_VERSION: ['0.10.0']
```

Which means that the following images are built and pushed:

* `registry.gitlab.com/jarvis-network/base/container-images/docker-buildx:latest` - same as 20.10
* `registry.gitlab.com/jarvis-network/base/container-images/docker-buildx:dind` - same as 20.10-dind
* `registry.gitlab.com/jarvis-network/base/container-images/docker-buildx:23.0.0-rc.1`
* `registry.gitlab.com/jarvis-network/base/container-images/docker-buildx:23.0.0-rc.1-dind`
* `registry.gitlab.com/jarvis-network/base/container-images/docker-buildx:20.10`
* `registry.gitlab.com/jarvis-network/base/container-images/docker-buildx:20.10-dind`


These container registry is public and you can pull these images without any additional setup:

```sh
docker pull registry.gitlab.com/jarvis-network/base/container-images/docker-buildx:20.10
```
