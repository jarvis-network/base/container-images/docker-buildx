ARG DOCKER_VERSION
ARG BUILDX_VERSION=0.7.0

FROM docker/buildx-bin:${BUILDX_VERSION} as bin

FROM docker:${DOCKER_VERSION}

COPY --from=bin /buildx /usr/libexec/docker/cli-plugins/docker-buildx
RUN docker buildx install
RUN docker buildx version
